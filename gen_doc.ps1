﻿# 注意文件格式，编码必须为UTF8-BOM

# 定义输出编码（对[Console]::WriteLine生效）
$OutputEncoding = [console]::InputEncoding = [console]::OutputEncoding = [Text.UTF8Encoding]::UTF8
# 遇到错误立即停止
$ErrorActionPreference = 'Stop'
# 配置项目根目录
$ProjectRootPath = (Resolve-Path "${PSScriptRoot}").Path

$docDir = "${ProjectRootPath}/doc"
$swaggerName = "由图AI识别服务通信协议.openapi.yaml"
$mdName = "由图AI识别服务通信协议.md"
$swaggerPath = "${docDir}/${swaggerName}"
$mdPath = "${docDir}/${mdName}"

# 生成
protoc -I"${ProjectRootPath}" `
    --openapiv2_out="${docDir}" `
    --openapiv2_opt=allow_merge=true `
    --openapiv2_opt=merge_file_name="tmp" `
    --openapiv2_opt=disable_default_errors=true `
    --openapiv2_opt=openapi_naming_strategy=simple `
    --openapiv2_opt=enums_as_ints=true `
    --openapiv2_opt=output_format=yaml `
    --openapiv2_opt=disable_service_tags=true `
    --openapiv2_opt=preserve_rpc_order=true `
    --doc_out="${docDir}" `
    --doc_opt="markdown,tmp.md" `
    "${ProjectRootPath}/server/PublicInterface.proto" `
    "${ProjectRootPath}/shared/PublicDefine.proto" `
    "${ProjectRootPath}/shared/SharedDefine.proto"

# 处理生成的文档
Move-Item -Path "${docDir}/tmp.swagger.yaml" -Destination $swaggerPath -Force
Move-Item -Path "${docDir}/tmp.md" -Destination $mdPath -Force

$content = (Get-Content "${mdPath}" -Encoding UTF8)
$content = $content -join "`n"
$content = $content -creplace "\r?\n\r?\n\*\*【已弃用】:\*\*", "<br> **【已弃用】:** <br>"
$content = $content -creplace "\r?\n\r?\n\*\*注意事项:\*\*", "<br> **注意事项:**"
$content = $content -creplace " 使用场景", "<br> 使用场景"
$content = $content -creplace "( \d\.)", "<br>`$1"
$content = $content -creplace "YoYoAiPublic 由图AI识别服务——稳定性功能", "### YoYoAiPublic 由图AI识别服务——稳定性功能"
$subIndex = $content.IndexOf("### YoYoAiPublic 由图AI识别服务——稳定性功能")
$content = $content.Substring($subIndex)
Set-Content -Path $mdPath -Value $content -Encoding UTF8

# 将服务端文件转成客户端文件
$serverPrivateProto = (Get-Content "${ProjectRootPath}/server/PrivateInterface.proto" -Encoding UTF8)
$serverPublicProto = (Get-Content "${ProjectRootPath}/server/PublicInterface.proto" -Encoding UTF8)
$serverPublicProtoContent = $serverPublicProto -join "`n"
$serverPublicProtoContent = $serverPublicProtoContent -creplace "import.*google.*;\n", ""
$serverPublicProtoContent = $serverPublicProtoContent -creplace " {4,}((?!option deprecated).)*\n", ""
$serverPublicProtoContent = $serverPublicProtoContent -creplace "\{\r?\n +\}", "{}"
Set-Content -Path "${ProjectRootPath}/client/PrivateInterface.proto" -Value $serverPrivateProto -Encoding UTF8
Set-Content -Path "${ProjectRootPath}/client/PublicInterface.proto" -Value $serverPublicProtoContent -Encoding UTF8

