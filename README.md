# YuTooAiPb 由图 AI 智能识别通信协议

[ProtoVersionBadge]: https://img.shields.io/badge/release-v1.4.14-blue
[ProtoVersionURL]: https://gitee.com/zj-yoyo-windows-team/yutooai-pb/tree/v1.4.14

[![Latest Release][ProtoVersionBadge]][ProtoVersionURL]
[![Latest Author](https://img.shields.io/badge/author-Bearki-9cf)](https://gtiee.com/bearki)

## 一、ProtoBuf 协议和 gRPC 通信框架说明

### 1、ProtoBuf 协议说明

&emsp;&emsp;ProtoBuf (Protocol Buffer) 是谷歌内部的混合语言数据标准。通过将结构化的数据进行序列化(串行化)，用于通讯协议、数据存储等领域的语言无关、平台无关、可扩展的序列化结构数据格式。如需了解更多 ProtoBuf 内容，请移步官网：谷歌[ProtoBuf 官网](https://developers.google.cn/protocol-buffers)，如您已经了解 ProtoBuf 的语法及使用规则，可以通过以下链接下载编译工具将.proto 文件编译成您需要的对应语言的源代码。
请选择您当前操作系统和系统位数

- [Windows 64 位](https://github.com/protocolbuffers/protobuf/releases/download/v3.20.1/protoc-3.20.1-win64.zip)
- [Windows 32 位](https://github.com/protocolbuffers/protobuf/releases/download/v3.20.1/protoc-3.20.1-win32.zip)
- [更多](https://github.com/protocolbuffers/protobuf/releases)

### 2、gRPC 通信框架说明

&emsp;&emsp;gRPC 是一个现代开源的高性能远程过程调用 (RPC) 框架，可以在任何环境中运行。它可以通过对负载平衡、跟踪、健康检查和身份验证的可插拔支持有效地连接数据中心内和跨数据中心的服务。它也适用于分布式计算的最后一英里，将设备、移动应用程序和浏览器连接到后端服务。如需了解 gRPC 相关信息，请移步官网：[gRPC 官网](https://www.grpc.io)。

## 二、使用`.proto`文件生成源码

### 1. 生成 C++源码

```bat
@REM 使用Msys2安装grpc及其插件
pacman -S mingw-w64-I.686-grpc mingw-w64-x86_64-grpc
@REM 将mingw路径配置到环境变量，方便使用
set MINGW64=C:\msys64\mingw64
@REM 生成源码
protoc -I. ^
--plugin=protoc-gen-grpc=%MINGW64%\bin\grpc_cpp_plugin.exe ^
--cpp_out=build/cpp ^
--grpc_out=build/cpp ^
shared/*.proto ^
client/*.proto
```

### 2. 生成 C#源码

- C#的源码生成我们推荐使用自动生成（将以下内容添加到`{project}.csproj`文件中）

```xml
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <GeneratePackageOnBuild>True</GeneratePackageOnBuild>
    <Protobuf_TouchMissingExpected>true</Protobuf_TouchMissingExpected>
  </PropertyGroup>

  <ItemGroup>
    <PackageReference Include="Google.Protobuf" Version="3.20.1" />
    <PackageReference Include="Grpc" Version="2.45.0" />
    <PackageReference Include="Grpc.Tools" Version="2.45.0">
      <PrivateAssets>all</PrivateAssets>
      <IncludeAssets>runtime; build; native; contentfiles; analyzers; buildtransitive</IncludeAssets>
    </PackageReference>
  </ItemGroup>

  <ItemGroup>
    <Protobuf ProtoRoot="." Include="client/*.proto;shared/*.proto" OutputDir="." CompileOutputs="false" />
  </ItemGroup>

</Project>
```

### 3. 生成 Go 源码

```bat
@REM 安装插件
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
@REM 生成源码
protoc -I. ^
--go_out=build/go ^
--go-grpc_out=build/go ^
shared/*.proto ^
client/*.proto
```

### 4. 生成 Dart 源码

```bat
@REM flutter安装插件或dart安装插件
flutter pub global activate protoc_plugin
dart pub global activate protoc_plugin
@REM 生成源码
protoc -I. ^
--dart_out=grpc:build/dart ^
shared/*.proto ^
client/*.proto
```

### 5. 生成 Node 源码

```bat
@REM 安装grpc插件
npm install grpc-tools --save-dev
@REM 生成源码
protoc -I. ^
--plugin=protoc-gen-grpc=node/node_modules/grpc-tools/bin/grpc_node_plugin.exe ^
--js_out=import_style=commonjs,binary:node/src ^
--grpc_out=build/node ^
shared/*.proto ^
client/*.proto
```

### 5. 生成 Java 源码

```bat
@REM 安装grpc插件，（需要自己下载）
https://search.maven.org/search?q=io.grpc%20protoc-gen-grpc-java
@REM 生成源码
protoc -I. \
--java_out=build/java \
--grpc-java_out=build/java \
shared/*.proto \
client/*.proto
```

## 三、相关文档

- [谷歌 Proto Buffer 文档](https://developers.google.cn/protocol-buffers/docs/overview)
- [谷歌 gRPC 文档](https://www.grpc.io/docs)
