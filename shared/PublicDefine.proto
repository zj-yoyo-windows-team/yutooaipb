syntax = "proto3";

// 由图AI识别服务——实验性功能
//
// 注意事项:
// 该功能为实验性功能，不对外提供，请勿使用
//
// Author: Bearki
// DateTime: 2022/5/5 14:00
package YoYo.YuTooAiPb.V1;

// 引入依赖protobuf
import "shared/SharedDefine.proto";

// 生成Golang文件的必要参数
option go_package = ".;yutooaipb";
// 生成C#文件的必要参数
option csharp_namespace = "YoYo.YuTooAiPb";
// 生成Java文件的必要参数
option java_multiple_files = true;
option java_package = "link.yoyo.yutooaipb";

// GetVersionReply 获取版本信息【响应体】
message GetVersionReply {
  // 响应状态
  PublicReply status = 1;
  // SDK版本
  string sdkVersion = 2;
  // 识别模型版本
  string identifyVersion = 3;
  // 匹配规则版本
  string matchingVersion = 4;
  // 相机版本
  string cameraVersion = 5;
  // 机器码版本
  string machineVersion = 6;
  // 识别模型子版本（生鲜-fresh、零食-snack、通用-general）
  string identifySubVersion = 7;
}

// ActivateRequest 设备激活【请求体】
message ActivateRequest {
  // 激活信息【必填】
  ActivateInfo activateInfo = 1;
}

// GetPerformanceModeReply 获取性能模式【响应体】
message GetPerformanceModeReply {
  // 响应状态
  PublicReply status = 1;
  // 性能模式
  PerformanceModeType mode = 2;
}

// SetPerformanceModeRequest 设置性能模式【请求体】
message SetPerformanceModeRequest {
  // 性能模式【必填】
  PerformanceModeType mode = 1;
}

// GetCameraListReply 获取相机列表【响应体】
message GetCameraListReply {
  // 响应状态
  PublicReply status = 1;
  // 选中的相机信息
  CameraInfo selectedCamera = 2;
  // 相机列表
  repeated CameraInfo cameraList = 3;
}

// OpenCameraRequest 打开相机【请求体】
message OpenCameraRequest {
  // 选中的相机信息【必填】
  CameraInfo selectedCamera = 1;
}

// SetCameraAreaRequest 配置识别区域【请求体】
message SetCameraAreaRequest {
  // 相机区域信息【必填】
  CameraAreaInfo cameraAreaInfo = 1;
}

// GetCameraAreaReply 获取识别区域【响应体】
message GetCameraAreaReply {
  // 响应状态
  PublicReply status = 1;
  // 相机区域信息
  CameraAreaInfo cameraAreaInfo = 2;
  // 相机分辨率宽度
  int32 totalWidth = 3;
  // 相机分辨率高度
  int32 totalHeight = 4;
  // 相机描述
  string cameradescription = 5;
}

// GetDeviceInfoReply 获取设备信息【响应体】
message GetDeviceInfoReply {
  // 响应状态
  PublicReply status = 1;
  // 是否为演示模式
  bool isDemo = 2;
  // 设备SN
  string deviceSn = 3;
  // 门店编码
  string shopCode = 4;
  // 激活信息
  ActivateInfo activateInfo = 5;
  // 激活过期时间
  uint64 activateExpireDate = 6;
  // 当前使用的产品模型信息
  repeated ProductModelInfo productModelList = 7;
  // 当前使用的产品模型信息
  ProductModelInfo productModel = 8;
  // 自定义LOGO
  string diyLogo = 9;
}

// EditShopInfoRequest 编辑门店信息【请求体】
message EditShopInfoRequest {
  // 门店信息【必填】
  ShopBaseInfo shopInfo = 1;
}

// PluSyncRequest 商品同步【请求体】
message PluSyncRequest {
  // 商品数据列表【必填】
  repeated PluStatusInfo pluList = 1;
  // 是否为演示数据，
  // 【已弃用】请使用mode字段
  bool isDemo = 2 [ deprecated = true ];
  // 商品同步模式【必填】
  PluSyncModeType mode = 3;
}

// GetCameraStreamRequest 获取相机流【请求体】
message GetCameraStreamRequest {
  // 推流帧率【必填】
  uint32 fps = 1;
}

// GetCameraStreamReply 获取相机流【响应体】
message GetCameraStreamReply {
  // 响应状态
  PublicReply status = 1;
  // 图片流
  bytes imgData = 2;
}

// PluMatchRequest 商品信息匹配【请求体】
message PluMatchRequest {
  // 商品信息列表【必填】
  repeated PluInfo pluList = 1;
}

// PluMatchReply 商品信息匹配【响应体】
message PluMatchReply {
  // 响应状态
  PublicReply status = 1;
  // 匹配结果列表
  repeated PluMatchInfo resultList = 2;
}

// GetStudyDataRequest 获取学习数据【请求体】
message GetStudyDataRequest {
  // 学习数据的导出模式【必填】
  GetStudyDataModeType mode = 1;
  // 上一次导出学习数据的时间（毫秒时间戳）【可选】
  int64 lastTimestamp = 2;
}

// GetStudyDataReply 获取学习数据【响应体】
message GetStudyDataReply {
  // 状态信息
  PublicReply status = 1;
  // 学习数据
  bytes studyData = 2;
  // 学习数据MD5
  string studyDataMd5 = 3;
  // 本次导出学习数据的时间（毫秒时间戳）
  int64 timestamp = 4;
}

// SetStudyDataRequest 配置学习数据【请求体】
message SetStudyDataRequest {
  // 学习数据操作模式【必填】
  SetStudyDataModeType mode = 1;
  // 学习数据【必填】
  bytes studyData = 2;
  // 学习数据MD5【可选】
  string studyDataMd5 = 3;
}

// StudyDataSyncRequest 同步学习数据【请求体】
message StudyDataSyncRequest {
  // 目标机器SDK服务IP地址【必填】
  string ip = 1;
  // 目标机器SDK服务端口【必填】
  uint32 port = 2;
  // 学习数据拉取模式【必填】
  GetStudyDataModeType pullMode = 3;
  // 学习数据导入模式【必填】
  SetStudyDataModeType importMode = 4;
}

// AiWeightJudgeRequest AI重量识别逻辑判断请求参数【请求体】
message AiWeightJudgeRequest {
  // AI重量识别逻辑判断模式【必填】
  AiWeightJudgeModeType mode = 1;
  // 重量（单位：克）【必填】
  int32 weight = 2;
  // 重量稳定状态【必填】
  WeightStatusType stable = 3;
  // 识别类型【必填】
  MatchingType matchType = 4;
}

// AiWeightJudgeReply AI重量识别逻辑判断结果【响应体】
message AiWeightJudgeReply {
  // 响应状态
  PublicReply status = 1;
  // 是否执行识别
  bool execIdentify = 2;
  // 批次ID
  int64 batchID = 3;
}

// AiMatchingRequest AI识别【请求体】
message AiMatchingRequest {
  // 最大输出数量(1<=maxNum<=10，默认: 5)【必填】
  int32 maxNum = 1;
  // 重量（单位：克）【必填】
  int32 weight = 2;
  // 重量稳定状态【可选：仅AiMatchingStream接口需要】
  WeightStatusType stable = 3;
  // 识别类型【可选：仅AiMatchingStream接口需要】
  MatchingType matchType = 4;
  // AI重量识别逻辑判断模式【可选：仅AiMatchingStream接口需要】
  AiWeightJudgeModeType mode = 5;
  // 批次ID【可选：仅AiMatching接口需要】
  int64 batchID = 6;
}

// AiMatchingReply AI识别【响应体】
message AiMatchingReply {
  // 响应状态
  PublicReply status = 1;
  // 传入的参数
  AiMatchingRequest aiMatchingRequest = 2;
  // 第几次识别
  uint32 matchIndex = 3;
  // 识别ID
  int64 identifyID = 4;
  // 识别结果商品列表
  repeated PluInfo pluList = 5;
}

// AiMarkRequest AI点选标记【请求体】
message AiMarkRequest {
  // 识别ID【必填】
  int64 identifyID = 1;
  // 标记方式【必填】
  MarkType markType = 2;
  // 商品信息【必填】
  PluInfo plu = 3;
  // 商品销售信息【可选】
  PluSaleInfo pluSaleInfo = 4;
}

// CorrectStudyDataRequest 矫正学习数据【请求体】
message CorrectStudyDataRequest {
  // 识别ID【必填，识别ID和识别图片路径必须二选一】
  int64 identifyID = 1;
  // 识别接口输出结果中需要删除的商品编码列表【必填】
  repeated PluInfo pluList = 2;
  // 识别图片路径【必填，识别ID和识别图片路径必须二选一】
  string identifyImgPath = 3;
}

// StudyModeMatchingReply 学习模式-识别【响应体】
message StudyModeMatchingReply {
  // 响应状态
  PublicReply status = 1;
  // 识别ID
  int64 identifyID = 2;
  // 图片流
  bytes imgData = 3;
}

// StudyModeMarkRequest 学习模式-保存【请求体】
message StudyModeMarkRequest {
  // 识别ID列表【必填】
  repeated int64 identifyIDList = 1;
  // 已标记的历史识别图片列表【可选】
  repeated string imgList = 2;
  // 商品信息【必填】
  PluInfo plu = 3;
  // 商品销售信息【可选】
  PluSaleInfo pluSaleInfo = 4;
}

// UpdatePluStatusRequest 更新商品状态信息【请求体】
message UpdatePluStatusRequest {
  // 商品状态列表【必填】
  repeated PluStatusInfo pluStatusList = 1;
}

// DeletePluDataRequest 删除指定商品数据【请求体】
message DeletePluDataRequest {
  // 商品列表【必填】
  repeated PluInfo pluList = 1;
}

// RemovePluStudyDataRequest 删除指定商品的学习数据【请求体】
message RemovePluStudyDataRequest {
  // 商品列表【必填】
  repeated PluInfo pluList = 1;
}

// SwitchLocalImageStudyRequest 启动、停止本地图片学习【请求体】
message SwitchLocalImageStudyRequest {
  // 启动或停止本地图片学习（0-停止，1-启动）【必填】
  uint32 mode = 1;
}

// LocalImageStudySpeedReply 本地图片学习的进度状态信息【响应体】
message LocalImageStudySpeedReply {
  // 响应状态
  PublicReply status = 1;
  // 必要学习进度信息
  ScheduleInfo scheduleInfo = 2;
  // 辅助学习进度信息
  ScheduleInfo helpScheduleInfo = 3;
}

// SwitchOutputOptRequest 开启、关闭输出优化【请求体】
message SwitchOutputOptRequest {
  option deprecated = true;
  // 是否开启输出优化【必填】
  bool open = 1;
}

// SwitchLiveMarkOptRequest 开启、关闭实时标记【请求体】
message SwitchLiveMarkOptRequest {
  option deprecated = true;
  // 是否开启实时标记【必填】
  bool open = 1;
}

// SwitchUnsaleOptRequest 设置批量下架的天数【请求体】
message SwitchUnsaleOptRequest {
  option deprecated = true;
  // 批量下架的天数，0表示关闭【必填】
  int32 day = 1;
}

// SwitchStdDiffOptRequest 设置标准差限制输出【请求体】
message SwitchStdDiffOptRequest {
  option deprecated = true;
  // 标准差值，取值范围[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5]，0表示关闭【必填】
  float val = 1;
}

// ChangeProductModelRequest 切换产品模型【请求体】
message ChangeProductModelRequest {
  // 产品模型信息【必填】
  ProductModelInfo productModelInfo = 1;
}

// LocalSyncGroupInfoModeType 本地同步分组信息操作模式
enum LocalSyncGroupInfoModeType {
  // 创建本地同步分组（自定义分组名称不能为空）
  CreateGroup = 0;
  // 加入本地同步分组（分组KEY和分组名称不能为空）
  JoinGroup = 1;
  // 退出本地同步分组（分组KEY不能为空）
  ExitGroup = 2;
  // 配置本地同步分组的自定义名称（分组自定义名称不能为空）
  SetGroupName = 3;
  // 配置本地同步分组数据同步标识（本地同步分组数据同步标识不能为空）
  SetGroupFlag = 4;
}

// SetLocalSyncGroupInfoRequest 设置本机的本地同步分组信息【请求体】
message SetLocalSyncGroupInfoRequest {
  // 本地同步分组信息操作模式【必填】
  LocalSyncGroupInfoModeType mode = 1;
  // 分组数据同步标识【必填】
  LocalSyncGroupFlagType groupFlag = 2;
  // 本地同步分组唯一KEY【可选】
  string groupKey = 3;
  // 本地同步分组的自定义名称【可选】
  string groupName = 4;
}

// GetLocalSyncGroupInfoReply 获取本地同步分组信息【响应体】
message GetLocalSyncGroupInfoReply {
  // 响应状态
  PublicReply status = 1;
  // 本机本地同步分组信息
  LocalSyncHostInfo localHostInfo = 2;
  // 本地同步分组信息列表
  repeated LocalSyncGroupInfo groupInfoList = 3;
}

// GetLocalSyncHostInfoReply 获取本地同步主机信息【响应体】
message GetLocalSyncHostInfoReply {
  // 响应状态
  PublicReply status = 1;
  // 发现本地同步主机信息列表
  repeated LocalSyncHostInfo hostList = 2;
}

// ExpandParamsKeyType 扩展参数键名
enum ExpandParamsKeyType {
  // 最小识别重量【值字段：uint32Val】（单位：克，最小：1，默认：20）
  MinIdentifyWeight = 0;
  // 匹配算法输出优化开关【值字段：boolVal】（true：开，false：关）
  MatchingOutOptimiza = 1;
  // 匹配算法实时标记开关【值字段：boolVal】（true：开，false：关）
  MatchingLiveMark = 2;
  // 匹配算法未打秤商品自动下架天数【值字段：uint32Val】（0：关闭，>0：自动下架天数）
  MatchingUnsaleDayNum = 3;
  // 匹配算法输出标准差限制【值字段：float32Val】（0：关闭，范围：[0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5,
  // 4, 4.5, 5]）
  MatchingStdDiffLimit = 4;
}

// SetExpandParamsRequest 设置扩展参数【请求体】
message SetExpandParamsRequest {
  // 扩展参数键名【必填】
  ExpandParamsKeyType key = 1;
  // 扩展参数值【必填】
  oneof val {
    // 字符串值【可选】
    string strVal = 2;
    // 有符号短整型值【可选】
    int32 int32Val = 3;
    // 有符号长整型值【可选】
    int64 int64Val = 4;
    // 无符号短整型值【可选】
    uint32 uint32Val = 5;
    // 无符号长整型值【可选】
    uint64 uint64Val = 6;
    // 单精度浮点值【可选】
    float float32Val = 7;
    // 双精度浮点值【可选】
    double float64Val = 8;
    // 布尔类型值【可选】
    bool boolVal = 9;
    // 字节数组类型值【可选】
    bytes bytesVal = 10;
  }
}

// GetExpandParamsRequest 查询扩展参数【请求体】
message GetExpandParamsRequest {
  // 扩展参数键名【必填】
  ExpandParamsKeyType key = 1;
}

// GetExpandParamsReply 查询扩展参数【响应体】
message GetExpandParamsReply {
  // 响应状态
  PublicReply status = 1;
  // 扩展参数值【必填】
  oneof val {
    // 字符串值
    string strVal = 2;
    // 有符号短整型值
    int32 int32Val = 3;
    // 有符号长整型值
    int64 int64Val = 4;
    // 无符号短整型值
    uint32 uint32Val = 5;
    // 无符号长整型值
    uint64 uint64Val = 6;
    // 单精度浮点值
    float float32Val = 7;
    // 双精度浮点值
    double float64Val = 8;
    // 布尔类型值
    bool boolVal = 9;
    // 字节数组类型值
    bytes bytesVal = 10;
  }
}