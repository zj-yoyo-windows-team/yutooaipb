﻿### YoYoAiPublic 由图AI识别服务——稳定性功能

| Method Name | Request Type | Response Type | Description |
| ----------- | ------------ | ------------- | ------------|
| GetVersion | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetVersionReply](#YoYo-YuTooAiPb-V1-GetVersionReply) | GetVersion 获取版本信息<br> **注意事项:**<br> 1.该接口在初始化之前可调用<br> 2.初始化之前调用该接口无法获取到识别算法的版本信息 |
| Init | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | Init 服务初始化接口<br> **注意事项:**<br> 1.建议返回状态成功后不再调用本接口 |
| UnInit | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | UnInit 服务释放接口<br> **注意事项:**<br> 1.程序退出前务必调用一次该接口，否则部分学习数据可能丢失 |
| Activate | [ActivateRequest](#YoYo-YuTooAiPb-V1-ActivateRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | Activate 设备激活<br> **注意事项:**<br> 1.建议仅在Init接口返回未激活状态码时调用该接口 |
| PluSync | [PluSyncRequest](#YoYo-YuTooAiPb-V1-PluSyncRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | PluSync 商品同步<br> **注意事项:**<br> 1.商品数据同支持全量同步和增量同步，默认为全量同步 |
| GetPerformanceMode | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetPerformanceModeReply](#YoYo-YuTooAiPb-V1-GetPerformanceModeReply) | GetPerformanceMode 获取性能模式<br> **注意事项:**<br> 1.该接口为获取当前算法使用的性能模式 |
| SetPerformanceMode | [SetPerformanceModeRequest](#YoYo-YuTooAiPb-V1-SetPerformanceModeRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SetPerformanceMode 设置性能模式<br> **注意事项:**<br> 1.该接口为配置当前算法使用的性能模式<br> 2.当识别功能正常时不建议修改性能模式 |
| ShowCameraAreaView | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | ShowCameraAreaView 渲染相机识别区域视图<br> **注意事项:**<br> 1.该接口已将GetCameraList，OpenCamera，GetCameraImage，GetCameraArea，SetCameraArea五个接口封装成内置UI<br> 2.调用该接口将会通过弹窗方式打开UI |
| GetCameraList | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetCameraListReply](#YoYo-YuTooAiPb-V1-GetCameraListReply) | GetCameraList 获取相机列表<br> **注意事项:**<br> 1.调用GetCameraList获取相机列表以及正在使用的相机<br> 2.调用OpenCamera接口打开相机列表中的某个相机 |
| OpenCamera | [OpenCameraRequest](#YoYo-YuTooAiPb-V1-OpenCameraRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | OpenCamera 打开相机<br> **注意事项:**<br> 1.调用GetCameraList获取相机列表以及正在使用的相机<br> 2.调用OpenCamera接口打开相机列表中的某个相机 |
| GetCameraArea | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetCameraAreaReply](#YoYo-YuTooAiPb-V1-GetCameraAreaReply) | GetCameraArea 获取识别区域<br> **注意事项:**<br> 1.调用GetCameraArea接口获取已存在的相机区域坐标<br> 2.调用GetCameraImage或GetCameraStream接口渲染图像<br> 3.在图像上显示区域坐标，注意渲染比例和真实比例的换算<br> 4.调整合适的识别区域，将渲染比例换算为真实比例<br> 5.调用SetCameraArea接口，将真实比例传入进行配置 |
| SetCameraArea | [SetCameraAreaRequest](#YoYo-YuTooAiPb-V1-SetCameraAreaRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SetCameraArea 配置识别区域<br> **注意事项:**<br> 1.调用GetCameraArea接口获取已存在的相机区域坐标<br> 2.调用GetCameraImage或GetCameraStream接口渲染图像<br> 3.在图像上显示区域坐标，注意渲染比例和真实比例的换算<br> 4.调整合适的识别区域，将渲染比例换算为真实比例<br> 5.调用SetCameraArea接口，将真实比例传入进行配置 |
| GetDeviceInfo | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetDeviceInfoReply](#YoYo-YuTooAiPb-V1-GetDeviceInfoReply) | GetDeviceInfo 获取设备信息<br> **注意事项:**<br> 1.该接口能获取到设备和门店的基本信息 |
| EditShopInfo | [EditShopInfoRequest](#YoYo-YuTooAiPb-V1-EditShopInfoRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | EditShopInfo 编辑门店信息<br> **注意事项:**<br> 1.该接口能修改门店的基本信息 |
| GetCameraImage | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetCameraStreamReply](#YoYo-YuTooAiPb-V1-GetCameraStreamReply) | GetCameraImage 获取相机图片（JPEG）<br> **注意事项:**<br> 1.该接口为GetCameraStream接口的一元版本<br> 2.如果对流式接口对接不便或不了解，建议使用该接口 |
| GetCameraStream | [GetCameraStreamRequest](#YoYo-YuTooAiPb-V1-GetCameraStreamRequest) | [GetCameraStreamReply](#YoYo-YuTooAiPb-V1-GetCameraStreamReply) stream | GetCameraStream 获取相机流（JPEG）【流式】<br> **注意事项:**<br> 1.该接口为流式接口<br> 2.如果对流式接口对接不便或不了解，建议使用GetCameraImage接口 |
| PluMatch | [PluMatchRequest](#YoYo-YuTooAiPb-V1-PluMatchRequest) | [PluMatchReply](#YoYo-YuTooAiPb-V1-PluMatchReply) | PluMatch 商品信息匹配<br> **注意事项:**<br> 1.该接口为批量匹配商品信息接口，并非识别接口<br> 2.为考虑性能，建议500~1000个商品传入该接口进行匹配 |
| GetStudyData | [GetStudyDataRequest](#YoYo-YuTooAiPb-V1-GetStudyDataRequest) | [GetStudyDataReply](#YoYo-YuTooAiPb-V1-GetStudyDataReply) | GetStudyData 获取学习数据<br> **注意事项:**<br> 1.在当前机器上调用GetStudyData接口获取学习数据<br> 2.将通过GetStudyData接口获取到的学习数据传输到其他机器<br> 3.将收到的学习数据传入SetStudyData接口，通过指定模式操作学习数据 |
| SetStudyData | [SetStudyDataRequest](#YoYo-YuTooAiPb-V1-SetStudyDataRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SetStudyData 配置学习数据<br> **注意事项:**<br> 1.在其它机器上调用GetStudyData接口获取学习数据<br> 2.将在其他机器上通过GetStudyData接口获取到的学习数据传入本机<br> 3.将收到的学习数据传入SetStudyData接口，通过指定模式操作学习数据 |
| StudyDataSync | [StudyDataSyncRequest](#YoYo-YuTooAiPb-V1-StudyDataSyncRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | StudyDataSync 学习数据同步<br> **注意事项:**<br> 1.该接口实现了GetStudyData、SetStudyData接口的组合<br> 2.实现了局域网内（IP&#43;端口）的学习数据同步功能<br> 3.当拉取模式选择了增量模式时，导入模式禁止选择覆盖模式 |
| AiWeightJudge | [AiWeightJudgeRequest](#YoYo-YuTooAiPb-V1-AiWeightJudgeRequest) | [AiWeightJudgeReply](#YoYo-YuTooAiPb-V1-AiWeightJudgeReply) | AiWeightJudge AI重量识别逻辑判断<br> **注意事项:**<br> 1.应接口应配合AiMatching接口一起使用<br> 2.禁止和AiMatchingStream接口一起使用<br> 3.重量传入需要定时传入，就算取重错误也应该传入,否则会造成重量推算不正确 |
| AiMatching | [AiMatchingRequest](#YoYo-YuTooAiPb-V1-AiMatchingRequest) | [AiMatchingReply](#YoYo-YuTooAiPb-V1-AiMatchingReply) | AiMatching AI识别<br> **注意事项:**<br> 1.先调用AiMatching或AiMatchingStream接口获取识别结果<br> 2.如果对流式接口对接不便或不了解，建议使用该接口<br> 3.调用AiMark接口对识别结果进行调校<br> 4.当你对在什么时候执行识别感到困惑的时候，请使用AiWeightJudge接口 |
| AiMatchingStream | [AiMatchingRequest](#YoYo-YuTooAiPb-V1-AiMatchingRequest) stream | [AiMatchingReply](#YoYo-YuTooAiPb-V1-AiMatchingReply) stream | AiMatchingStream AI识别【流式】<br> **注意事项:**<br> 1.先调用AiMatching或AiMatchingStream接口获取识别结果<br> 2.如果对流式接口对接不便或不了解，建议使用AiMatching接口<br> 3.调用AiMark接口对识别结果进行调校 |
| AiMark | [AiMarkRequest](#YoYo-YuTooAiPb-V1-AiMarkRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | AiMark 点选标记<br> **注意事项:**<br> 1.先调用AiMatching或AiMatchingStream接口获取识别结果<br> 2.调用AiMark接口对识别结果进行调校 |
| StudyModeMatching | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [StudyModeMatchingReply](#YoYo-YuTooAiPb-V1-StudyModeMatchingReply) | StudyModeMatching 学习模式-识别<br> **注意事项:**<br> 1.先调用StudyModeMatching接口获取识别ID<br> 2.将需要保存的识别ID传入StudyModeMark接口进行学习数据保存 |
| StudyModeMark | [StudyModeMarkRequest](#YoYo-YuTooAiPb-V1-StudyModeMarkRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | StudyModeMark 学习模式-保存<br> **注意事项:**<br> 1.先调用StudyModeMatching接口获取识别ID<br> 2.将需要保存的识别ID传入StudyModeMark接口进行学习数据保存 |
| CorrectStudyData | [CorrectStudyDataRequest](#YoYo-YuTooAiPb-V1-CorrectStudyDataRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | CorrectStudyData 矫正学习数据<br> **注意事项:**<br> 1.先调用AiMatching或AiMatchingStream接口获取识别结果<br> 2.果对流式接口对接不便或不了解，建议使用该接口<br> 3.将想移除的识别结果传入该接口进行学习数据矫正 |
| UpdatePluStatus | [UpdatePluStatusRequest](#YoYo-YuTooAiPb-V1-UpdatePluStatusRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | UpdatePluStatus 更新商品状态（请通过PluSync接口增量同步方式进行更新）<br> **注意事项:**<br> 1.该接口会修改商品的状态信息，锁定和下架的商品将不会出现在识别结果中<br> 2.已下架的商品通过Mark后会自动上架 |
| DeletePluData | [DeletePluDataRequest](#YoYo-YuTooAiPb-V1-DeletePluDataRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | DeletePluData 删除指定商品数据<br> **注意事项:**<br> 1.该接口会删除传入商品的所有数据，包含学习数据<br> 2.该接口入参仅需为商品编码字段赋值即可，商品名称字段无需赋值 |
| RemovePluStudyData | [RemovePluStudyDataRequest](#YoYo-YuTooAiPb-V1-RemovePluStudyDataRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | RemovePluStudyData 移除指定商品的学习数据<br> **注意事项:**<br> 1.该接口会删除传入商品的学习数据，商品任然会保留<br> 2.该接口入参仅需为商品编码字段赋值即可，商品名称字段无需赋值 |
| SwitchLocalImageStudy | [SwitchLocalImageStudyRequest](#YoYo-YuTooAiPb-V1-SwitchLocalImageStudyRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SwitchLocalImageStudy 启动、停止本地图片学习<br> **注意事项:**<br> 1.请确保本地data目录已存在点选标记图片 |
| LocalImageStudySpeed | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [LocalImageStudySpeedReply](#YoYo-YuTooAiPb-V1-LocalImageStudySpeedReply) | LocalImageStudySpeed 获取本地图片学习的进度状态信息<br> **注意事项:**<br> 1.当状态返回未成功时请及时终止该接口的调用 |
| ShowLocalImageStudyView | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | ShowLocalImageStudyView 渲染本地图片学习进度视图<br> **注意事项:**<br> 1.调用该接口前需要先同步商品数据，否则不存在的商品将不会进行学习<br> 2.该接口已将SwitchLocalImageStudy，LocalImageStudySpeed两个接口封装成内置UI<br> 3.调用该接口将会通过弹窗方式打开UI |
| SwitchOutputOpt | [SwitchOutputOptRequest](#YoYo-YuTooAiPb-V1-SwitchOutputOptRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SwitchOutputOpt 学习模型参数设置——开启、关闭输出优化<br> **【已弃用】:** <br> 请使用SetExpandParams、GetExpandParams实现相同功能<br> **注意事项:**<br> 1.该接口可调整输出结果的准确性，目前处于公测阶段（不建议使用） |
| SwitchLiveMarkOpt | [SwitchLiveMarkOptRequest](#YoYo-YuTooAiPb-V1-SwitchLiveMarkOptRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SwitchLiveMarkOpt 学习模型参数设置——开启、关闭实时标记<br> **【已弃用】:** <br> 请使用SetExpandParams、GetExpandParams实现相同功能<br> **注意事项:**<br> 1.该接口可防止任意人员的错误标记，目前处于公测阶段（不建议使用） |
| SwitchUnsaleOpt | [SwitchUnsaleOptRequest](#YoYo-YuTooAiPb-V1-SwitchUnsaleOptRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SwitchUnsaleOpt 学习模型参数设置——批量下架的天数<br> **【已弃用】:** <br> 请使用SetExpandParams、GetExpandParams实现相同功能<br> **注意事项:**<br> 1.目前处于公测阶段（不建议使用） |
| SwitchStdDiffOpt | [SwitchStdDiffOptRequest](#YoYo-YuTooAiPb-V1-SwitchStdDiffOptRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SwitchStdDiffOpt 学习模型参数设置——标准差限制输出<br> **【已弃用】:** <br> 请使用SetExpandParams、GetExpandParams实现相同功能<br> **注意事项:**<br> 1.目前处于公测阶段（不建议使用） |
| ChangeProductModel | [ChangeProductModelRequest](#YoYo-YuTooAiPb-V1-ChangeProductModelRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | ChangeProductModel 切换产品级别和AI模型<br> **注意事项:**<br> 1.请先调用GetDeviceInfo接口获取产品模型列表<br> 2.传入选中的产品模型信息，非范围内的产品模型信息将会返回失败<br> 3.产品模型切换后需要重启整个应用才会生效 |
| SetLocalSyncGroupInfo | [SetLocalSyncGroupInfoRequest](#YoYo-YuTooAiPb-V1-SetLocalSyncGroupInfoRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SetLocalSyncGroupInfo 设置本机本地同步分组信息<br> **注意事项:**<br> 1.仅在本机未加入任何分组的情况下可以创建或加入分组<br> 2.仅在分组为本机创建时可以修改分组的自定义名称 |
| GetLocalSyncGroupInfo | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetLocalSyncGroupInfoReply](#YoYo-YuTooAiPb-V1-GetLocalSyncGroupInfoReply) | GetLocalSyncGroupInfo 获取本地同步分组信息<br> **注意事项:**<br> 1.本地同步分组信息仅本机所在的分组有在线状态检测，其他分组的设备不存在离线检测 |
| GetLocalSyncHostInfo | [EmptyRequest](#YoYo-YuTooAiPb-V1-EmptyRequest) | [GetLocalSyncHostInfoReply](#YoYo-YuTooAiPb-V1-GetLocalSyncHostInfoReply) | GetLocalSyncHostInfo 获取本地同步主机信息<br> **注意事项:**<br> 1.返回当前可发现的局域网设备列表 |
| SetExpandParams | [SetExpandParamsRequest](#YoYo-YuTooAiPb-V1-SetExpandParamsRequest) | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) | SetExpandParams 配置扩展参数<br> **注意事项:**<br> 1.配置当前受支持的扩展参数 |
| GetExpandParams | [GetExpandParamsRequest](#YoYo-YuTooAiPb-V1-GetExpandParamsRequest) | [GetExpandParamsReply](#YoYo-YuTooAiPb-V1-GetExpandParamsReply) | GetExpandParams 查询扩展参数<br> **注意事项:**<br> 1.查询当前受支持的扩展参数 |

 



<a name="shared_PublicDefine-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## shared/PublicDefine.proto



<a name="YoYo-YuTooAiPb-V1-ActivateRequest"></a>

### ActivateRequest
ActivateRequest 设备激活【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| activateInfo | [ActivateInfo](#YoYo-YuTooAiPb-V1-ActivateInfo) |  | 激活信息【必填】 |






<a name="YoYo-YuTooAiPb-V1-AiMarkRequest"></a>

### AiMarkRequest
AiMarkRequest AI点选标记【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| identifyID | [int64](#int64) |  | 识别ID【必填】 |
| markType | [MarkType](#YoYo-YuTooAiPb-V1-MarkType) |  | 标记方式【必填】 |
| plu | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) |  | 商品信息【必填】 |
| pluSaleInfo | [PluSaleInfo](#YoYo-YuTooAiPb-V1-PluSaleInfo) |  | 商品销售信息【可选】 |






<a name="YoYo-YuTooAiPb-V1-AiMatchingReply"></a>

### AiMatchingReply
AiMatchingReply AI识别【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| aiMatchingRequest | [AiMatchingRequest](#YoYo-YuTooAiPb-V1-AiMatchingRequest) |  | 传入的参数 |
| matchIndex | [uint32](#uint32) |  | 第几次识别 |
| identifyID | [int64](#int64) |  | 识别ID |
| pluList | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) | repeated | 识别结果商品列表 |






<a name="YoYo-YuTooAiPb-V1-AiMatchingRequest"></a>

### AiMatchingRequest
AiMatchingRequest AI识别【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| maxNum | [int32](#int32) |  | 最大输出数量(1&lt;=maxNum&lt;=10，默认: 5)【必填】 |
| weight | [int32](#int32) |  | 重量（单位：克）【必填】 |
| stable | [WeightStatusType](#YoYo-YuTooAiPb-V1-WeightStatusType) |  | 重量稳定状态【可选：仅AiMatchingStream接口需要】 |
| matchType | [MatchingType](#YoYo-YuTooAiPb-V1-MatchingType) |  | 识别类型【可选：仅AiMatchingStream接口需要】 |
| mode | [AiWeightJudgeModeType](#YoYo-YuTooAiPb-V1-AiWeightJudgeModeType) |  | AI重量识别逻辑判断模式【可选：仅AiMatchingStream接口需要】 |
| batchID | [int64](#int64) |  | 批次ID【可选：仅AiMatching接口需要】 |






<a name="YoYo-YuTooAiPb-V1-AiWeightJudgeReply"></a>

### AiWeightJudgeReply
AiWeightJudgeReply AI重量识别逻辑判断结果【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| execIdentify | [bool](#bool) |  | 是否执行识别 |
| batchID | [int64](#int64) |  | 批次ID |






<a name="YoYo-YuTooAiPb-V1-AiWeightJudgeRequest"></a>

### AiWeightJudgeRequest
AiWeightJudgeRequest AI重量识别逻辑判断请求参数【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| mode | [AiWeightJudgeModeType](#YoYo-YuTooAiPb-V1-AiWeightJudgeModeType) |  | AI重量识别逻辑判断模式【必填】 |
| weight | [int32](#int32) |  | 重量（单位：克）【必填】 |
| stable | [WeightStatusType](#YoYo-YuTooAiPb-V1-WeightStatusType) |  | 重量稳定状态【必填】 |
| matchType | [MatchingType](#YoYo-YuTooAiPb-V1-MatchingType) |  | 识别类型【必填】 |






<a name="YoYo-YuTooAiPb-V1-ChangeProductModelRequest"></a>

### ChangeProductModelRequest
ChangeProductModelRequest 切换产品模型【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| productModelInfo | [ProductModelInfo](#YoYo-YuTooAiPb-V1-ProductModelInfo) |  | 产品模型信息【必填】 |






<a name="YoYo-YuTooAiPb-V1-CorrectStudyDataRequest"></a>

### CorrectStudyDataRequest
CorrectStudyDataRequest 矫正学习数据【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| identifyID | [int64](#int64) |  | 识别ID【必填，识别ID和识别图片路径必须二选一】 |
| pluList | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) | repeated | 识别接口输出结果中需要删除的商品编码列表【必填】 |
| identifyImgPath | [string](#string) |  | 识别图片路径【必填，识别ID和识别图片路径必须二选一】 |






<a name="YoYo-YuTooAiPb-V1-DeletePluDataRequest"></a>

### DeletePluDataRequest
DeletePluDataRequest 删除指定商品数据【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pluList | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) | repeated | 商品列表【必填】 |






<a name="YoYo-YuTooAiPb-V1-EditShopInfoRequest"></a>

### EditShopInfoRequest
EditShopInfoRequest 编辑门店信息【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| shopInfo | [ShopBaseInfo](#YoYo-YuTooAiPb-V1-ShopBaseInfo) |  | 门店信息【必填】 |






<a name="YoYo-YuTooAiPb-V1-GetCameraAreaReply"></a>

### GetCameraAreaReply
GetCameraAreaReply 获取识别区域【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| cameraAreaInfo | [CameraAreaInfo](#YoYo-YuTooAiPb-V1-CameraAreaInfo) |  | 相机区域信息 |
| totalWidth | [int32](#int32) |  | 相机分辨率宽度 |
| totalHeight | [int32](#int32) |  | 相机分辨率高度 |
| cameradescription | [string](#string) |  | 相机描述 |






<a name="YoYo-YuTooAiPb-V1-GetCameraListReply"></a>

### GetCameraListReply
GetCameraListReply 获取相机列表【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| selectedCamera | [CameraInfo](#YoYo-YuTooAiPb-V1-CameraInfo) |  | 选中的相机信息 |
| cameraList | [CameraInfo](#YoYo-YuTooAiPb-V1-CameraInfo) | repeated | 相机列表 |






<a name="YoYo-YuTooAiPb-V1-GetCameraStreamReply"></a>

### GetCameraStreamReply
GetCameraStreamReply 获取相机流【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| imgData | [bytes](#bytes) |  | 图片流 |






<a name="YoYo-YuTooAiPb-V1-GetCameraStreamRequest"></a>

### GetCameraStreamRequest
GetCameraStreamRequest 获取相机流【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| fps | [uint32](#uint32) |  | 推流帧率【必填】 |






<a name="YoYo-YuTooAiPb-V1-GetDeviceInfoReply"></a>

### GetDeviceInfoReply
GetDeviceInfoReply 获取设备信息【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| isDemo | [bool](#bool) |  | 是否为演示模式 |
| deviceSn | [string](#string) |  | 设备SN |
| shopCode | [string](#string) |  | 门店编码 |
| activateInfo | [ActivateInfo](#YoYo-YuTooAiPb-V1-ActivateInfo) |  | 激活信息 |
| activateExpireDate | [uint64](#uint64) |  | 激活过期时间 |
| productModelList | [ProductModelInfo](#YoYo-YuTooAiPb-V1-ProductModelInfo) | repeated | 当前使用的产品模型信息 |
| productModel | [ProductModelInfo](#YoYo-YuTooAiPb-V1-ProductModelInfo) |  | 当前使用的产品模型信息 |
| diyLogo | [string](#string) |  | 自定义LOGO |






<a name="YoYo-YuTooAiPb-V1-GetExpandParamsReply"></a>

### GetExpandParamsReply
GetExpandParamsReply 查询扩展参数【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| strVal | [string](#string) |  | 字符串值 |
| int32Val | [int32](#int32) |  | 有符号短整型值 |
| int64Val | [int64](#int64) |  | 有符号长整型值 |
| uint32Val | [uint32](#uint32) |  | 无符号短整型值 |
| uint64Val | [uint64](#uint64) |  | 无符号长整型值 |
| float32Val | [float](#float) |  | 单精度浮点值 |
| float64Val | [double](#double) |  | 双精度浮点值 |
| boolVal | [bool](#bool) |  | 布尔类型值 |
| bytesVal | [bytes](#bytes) |  | 字节数组类型值 |






<a name="YoYo-YuTooAiPb-V1-GetExpandParamsRequest"></a>

### GetExpandParamsRequest
GetExpandParamsRequest 查询扩展参数【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [ExpandParamsKeyType](#YoYo-YuTooAiPb-V1-ExpandParamsKeyType) |  | 扩展参数键名【必填】 |






<a name="YoYo-YuTooAiPb-V1-GetLocalSyncGroupInfoReply"></a>

### GetLocalSyncGroupInfoReply
GetLocalSyncGroupInfoReply 获取本地同步分组信息【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| localHostInfo | [LocalSyncHostInfo](#YoYo-YuTooAiPb-V1-LocalSyncHostInfo) |  | 本机本地同步分组信息 |
| groupInfoList | [LocalSyncGroupInfo](#YoYo-YuTooAiPb-V1-LocalSyncGroupInfo) | repeated | 本地同步分组信息列表 |






<a name="YoYo-YuTooAiPb-V1-GetLocalSyncHostInfoReply"></a>

### GetLocalSyncHostInfoReply
GetLocalSyncHostInfoReply 获取本地同步主机信息【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| hostList | [LocalSyncHostInfo](#YoYo-YuTooAiPb-V1-LocalSyncHostInfo) | repeated | 发现本地同步主机信息列表 |






<a name="YoYo-YuTooAiPb-V1-GetPerformanceModeReply"></a>

### GetPerformanceModeReply
GetPerformanceModeReply 获取性能模式【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| mode | [PerformanceModeType](#YoYo-YuTooAiPb-V1-PerformanceModeType) |  | 性能模式 |






<a name="YoYo-YuTooAiPb-V1-GetStudyDataReply"></a>

### GetStudyDataReply
GetStudyDataReply 获取学习数据【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 状态信息 |
| studyData | [bytes](#bytes) |  | 学习数据 |
| studyDataMd5 | [string](#string) |  | 学习数据MD5 |
| timestamp | [int64](#int64) |  | 本次导出学习数据的时间（毫秒时间戳） |






<a name="YoYo-YuTooAiPb-V1-GetStudyDataRequest"></a>

### GetStudyDataRequest
GetStudyDataRequest 获取学习数据【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| mode | [GetStudyDataModeType](#YoYo-YuTooAiPb-V1-GetStudyDataModeType) |  | 学习数据的导出模式【必填】 |
| lastTimestamp | [int64](#int64) |  | 上一次导出学习数据的时间（毫秒时间戳）【可选】 |






<a name="YoYo-YuTooAiPb-V1-GetVersionReply"></a>

### GetVersionReply
GetVersionReply 获取版本信息【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| sdkVersion | [string](#string) |  | SDK版本 |
| identifyVersion | [string](#string) |  | 识别模型版本 |
| matchingVersion | [string](#string) |  | 匹配规则版本 |
| cameraVersion | [string](#string) |  | 相机版本 |
| machineVersion | [string](#string) |  | 机器码版本 |
| identifySubVersion | [string](#string) |  | 识别模型子版本（生鲜-fresh、零食-snack、通用-general） |






<a name="YoYo-YuTooAiPb-V1-LocalImageStudySpeedReply"></a>

### LocalImageStudySpeedReply
LocalImageStudySpeedReply 本地图片学习的进度状态信息【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| scheduleInfo | [ScheduleInfo](#YoYo-YuTooAiPb-V1-ScheduleInfo) |  | 必要学习进度信息 |
| helpScheduleInfo | [ScheduleInfo](#YoYo-YuTooAiPb-V1-ScheduleInfo) |  | 辅助学习进度信息 |






<a name="YoYo-YuTooAiPb-V1-OpenCameraRequest"></a>

### OpenCameraRequest
OpenCameraRequest 打开相机【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| selectedCamera | [CameraInfo](#YoYo-YuTooAiPb-V1-CameraInfo) |  | 选中的相机信息【必填】 |






<a name="YoYo-YuTooAiPb-V1-PluMatchReply"></a>

### PluMatchReply
PluMatchReply 商品信息匹配【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| resultList | [PluMatchInfo](#YoYo-YuTooAiPb-V1-PluMatchInfo) | repeated | 匹配结果列表 |






<a name="YoYo-YuTooAiPb-V1-PluMatchRequest"></a>

### PluMatchRequest
PluMatchRequest 商品信息匹配【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pluList | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) | repeated | 商品信息列表【必填】 |






<a name="YoYo-YuTooAiPb-V1-PluSyncRequest"></a>

### PluSyncRequest
PluSyncRequest 商品同步【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pluList | [PluStatusInfo](#YoYo-YuTooAiPb-V1-PluStatusInfo) | repeated | 商品数据列表【必填】 |
| isDemo | [bool](#bool) |  | **Deprecated.** 是否为演示数据， 【已弃用】请使用mode字段 |
| mode | [PluSyncModeType](#YoYo-YuTooAiPb-V1-PluSyncModeType) |  | 商品同步模式【必填】 |






<a name="YoYo-YuTooAiPb-V1-RemovePluStudyDataRequest"></a>

### RemovePluStudyDataRequest
RemovePluStudyDataRequest 删除指定商品的学习数据【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pluList | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) | repeated | 商品列表【必填】 |






<a name="YoYo-YuTooAiPb-V1-SetCameraAreaRequest"></a>

### SetCameraAreaRequest
SetCameraAreaRequest 配置识别区域【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cameraAreaInfo | [CameraAreaInfo](#YoYo-YuTooAiPb-V1-CameraAreaInfo) |  | 相机区域信息【必填】 |






<a name="YoYo-YuTooAiPb-V1-SetExpandParamsRequest"></a>

### SetExpandParamsRequest
SetExpandParamsRequest 设置扩展参数【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| key | [ExpandParamsKeyType](#YoYo-YuTooAiPb-V1-ExpandParamsKeyType) |  | 扩展参数键名【必填】 |
| strVal | [string](#string) |  | 字符串值【可选】 |
| int32Val | [int32](#int32) |  | 有符号短整型值【可选】 |
| int64Val | [int64](#int64) |  | 有符号长整型值【可选】 |
| uint32Val | [uint32](#uint32) |  | 无符号短整型值【可选】 |
| uint64Val | [uint64](#uint64) |  | 无符号长整型值【可选】 |
| float32Val | [float](#float) |  | 单精度浮点值【可选】 |
| float64Val | [double](#double) |  | 双精度浮点值【可选】 |
| boolVal | [bool](#bool) |  | 布尔类型值【可选】 |
| bytesVal | [bytes](#bytes) |  | 字节数组类型值【可选】 |






<a name="YoYo-YuTooAiPb-V1-SetLocalSyncGroupInfoRequest"></a>

### SetLocalSyncGroupInfoRequest
SetLocalSyncGroupInfoRequest 设置本机的本地同步分组信息【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| mode | [LocalSyncGroupInfoModeType](#YoYo-YuTooAiPb-V1-LocalSyncGroupInfoModeType) |  | 本地同步分组信息操作模式【必填】 |
| groupFlag | [LocalSyncGroupFlagType](#YoYo-YuTooAiPb-V1-LocalSyncGroupFlagType) |  | 分组数据同步标识【必填】 |
| groupKey | [string](#string) |  | 本地同步分组唯一KEY【可选】 |
| groupName | [string](#string) |  | 本地同步分组的自定义名称【可选】 |






<a name="YoYo-YuTooAiPb-V1-SetPerformanceModeRequest"></a>

### SetPerformanceModeRequest
SetPerformanceModeRequest 设置性能模式【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| mode | [PerformanceModeType](#YoYo-YuTooAiPb-V1-PerformanceModeType) |  | 性能模式【必填】 |






<a name="YoYo-YuTooAiPb-V1-SetStudyDataRequest"></a>

### SetStudyDataRequest
SetStudyDataRequest 配置学习数据【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| mode | [SetStudyDataModeType](#YoYo-YuTooAiPb-V1-SetStudyDataModeType) |  | 学习数据操作模式【必填】 |
| studyData | [bytes](#bytes) |  | 学习数据【必填】 |
| studyDataMd5 | [string](#string) |  | 学习数据MD5【可选】 |






<a name="YoYo-YuTooAiPb-V1-StudyDataSyncRequest"></a>

### StudyDataSyncRequest
StudyDataSyncRequest 同步学习数据【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| ip | [string](#string) |  | 目标机器SDK服务IP地址【必填】 |
| port | [uint32](#uint32) |  | 目标机器SDK服务端口【必填】 |
| pullMode | [GetStudyDataModeType](#YoYo-YuTooAiPb-V1-GetStudyDataModeType) |  | 学习数据拉取模式【必填】 |
| importMode | [SetStudyDataModeType](#YoYo-YuTooAiPb-V1-SetStudyDataModeType) |  | 学习数据导入模式【必填】 |






<a name="YoYo-YuTooAiPb-V1-StudyModeMarkRequest"></a>

### StudyModeMarkRequest
StudyModeMarkRequest 学习模式-保存【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| identifyIDList | [int64](#int64) | repeated | 识别ID列表【必填】 |
| imgList | [string](#string) | repeated | 已标记的历史识别图片列表【可选】 |
| plu | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) |  | 商品信息【必填】 |
| pluSaleInfo | [PluSaleInfo](#YoYo-YuTooAiPb-V1-PluSaleInfo) |  | 商品销售信息【可选】 |






<a name="YoYo-YuTooAiPb-V1-StudyModeMatchingReply"></a>

### StudyModeMatchingReply
StudyModeMatchingReply 学习模式-识别【响应体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| status | [PublicReply](#YoYo-YuTooAiPb-V1-PublicReply) |  | 响应状态 |
| identifyID | [int64](#int64) |  | 识别ID |
| imgData | [bytes](#bytes) |  | 图片流 |






<a name="YoYo-YuTooAiPb-V1-SwitchLiveMarkOptRequest"></a>

### SwitchLiveMarkOptRequest
SwitchLiveMarkOptRequest 开启、关闭实时标记【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| open | [bool](#bool) |  | 是否开启实时标记【必填】 |






<a name="YoYo-YuTooAiPb-V1-SwitchLocalImageStudyRequest"></a>

### SwitchLocalImageStudyRequest
SwitchLocalImageStudyRequest 启动、停止本地图片学习【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| mode | [uint32](#uint32) |  | 启动或停止本地图片学习（0-停止，1-启动）【必填】 |






<a name="YoYo-YuTooAiPb-V1-SwitchOutputOptRequest"></a>

### SwitchOutputOptRequest
SwitchOutputOptRequest 开启、关闭输出优化【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| open | [bool](#bool) |  | 是否开启输出优化【必填】 |






<a name="YoYo-YuTooAiPb-V1-SwitchStdDiffOptRequest"></a>

### SwitchStdDiffOptRequest
SwitchStdDiffOptRequest 设置标准差限制输出【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| val | [float](#float) |  | 标准差值，取值范围[0,<br> 0.5, 1,<br> 1.5, 2,<br> 2.5, 3,<br> 3.5, 4,<br> 4.5, 5]，0表示关闭【必填】 |






<a name="YoYo-YuTooAiPb-V1-SwitchUnsaleOptRequest"></a>

### SwitchUnsaleOptRequest
SwitchUnsaleOptRequest 设置批量下架的天数【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| day | [int32](#int32) |  | 批量下架的天数，0表示关闭【必填】 |






<a name="YoYo-YuTooAiPb-V1-UpdatePluStatusRequest"></a>

### UpdatePluStatusRequest
UpdatePluStatusRequest 更新商品状态信息【请求体】


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pluStatusList | [PluStatusInfo](#YoYo-YuTooAiPb-V1-PluStatusInfo) | repeated | 商品状态列表【必填】 |





 


<a name="YoYo-YuTooAiPb-V1-ExpandParamsKeyType"></a>

### ExpandParamsKeyType
ExpandParamsKeyType 扩展参数键名

| Name | Number | Description |
| ---- | ------ | ----------- |
| MinIdentifyWeight | 0 | 最小识别重量【值字段：uint32Val】（单位：克，最小：1，默认：20） |
| MatchingOutOptimiza | 1 | 匹配算法输出优化开关【值字段：boolVal】（true：开，false：关） |
| MatchingLiveMark | 2 | 匹配算法实时标记开关【值字段：boolVal】（true：开，false：关） |
| MatchingUnsaleDayNum | 3 | 匹配算法未打秤商品自动下架天数【值字段：uint32Val】（0：关闭，&gt;0：自动下架天数） |
| MatchingStdDiffLimit | 4 | 匹配算法输出标准差限制【值字段：float32Val】（0：关闭，范围：[0,<br> 0.5, 1,<br> 1.5, 2,<br> 2.5, 3,<br> 3.5, 4,<br> 4.5, 5]） |



<a name="YoYo-YuTooAiPb-V1-LocalSyncGroupInfoModeType"></a>

### LocalSyncGroupInfoModeType
LocalSyncGroupInfoModeType 本地同步分组信息操作模式

| Name | Number | Description |
| ---- | ------ | ----------- |
| CreateGroup | 0 | 创建本地同步分组（自定义分组名称不能为空） |
| JoinGroup | 1 | 加入本地同步分组（分组KEY和分组名称不能为空） |
| ExitGroup | 2 | 退出本地同步分组（分组KEY不能为空） |
| SetGroupName | 3 | 配置本地同步分组的自定义名称（分组自定义名称不能为空） |
| SetGroupFlag | 4 | 配置本地同步分组数据同步标识（本地同步分组数据同步标识不能为空） |


 

 

 



<a name="shared_SharedDefine-proto"></a>
<p align="right"><a href="#top">Top</a></p>

## shared/SharedDefine.proto



<a name="YoYo-YuTooAiPb-V1-ActivateInfo"></a>

### ActivateInfo
ActivateInfo 激活信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| cdKey | [string](#string) |  | 激活码【必填】 |
| shopInfo | [ShopBaseInfo](#YoYo-YuTooAiPb-V1-ShopBaseInfo) |  | 门店信息【可选】 |
| shopCode | [string](#string) |  | 门店编码【可选】 |






<a name="YoYo-YuTooAiPb-V1-CameraAreaInfo"></a>

### CameraAreaInfo
CameraAreaInfo 相机区域信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| leftTopX | [int32](#int32) |  | 相机截取左上角X坐标【必填】 |
| leftTopY | [int32](#int32) |  | 相机截取左上角Y坐标【必填】 |
| rightTopX | [int32](#int32) |  | 相机截取右上角X坐标【必填】 |
| rightTopY | [int32](#int32) |  | 相机截取右上角Y坐标【必填】 |
| rightBottomX | [int32](#int32) |  | 相机截取右下角X坐标【必填】 |
| rightBottomY | [int32](#int32) |  | 相机截取右下角Y坐标【必填】 |
| leftBottomX | [int32](#int32) |  | 相机截取左下角X坐标【必填】 |
| leftBottomY | [int32](#int32) |  | 相机截取左下角Y坐标【必填】 |






<a name="YoYo-YuTooAiPb-V1-CameraInfo"></a>

### CameraInfo
CameraInfo 相机信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| id | [uint32](#uint32) |  | 相机ID【必填】 |
| name | [string](#string) |  | 相机名称【可选】 |
| symbolicLink | [string](#string) |  | 相机路径【可选】 |
| locationInfo | [string](#string) |  | 相机本地编码【可选】 |
| supportInfoList | [CameraSupportInfo](#YoYo-YuTooAiPb-V1-CameraSupportInfo) | repeated | 相机的支持（分辨率、帧率）信息列表【可选】 |
| supportInfo | [CameraSupportInfo](#YoYo-YuTooAiPb-V1-CameraSupportInfo) |  | 当前相机选中的支持（分辨率、帧率）信息（仅已打开的相机会返回该字段）【可选】 |






<a name="YoYo-YuTooAiPb-V1-CameraSupportInfo"></a>

### CameraSupportInfo
CameraSupportInfo 相机支持信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| width | [int32](#int32) |  | 相机支持的分辨率宽度【必填】 |
| height | [int32](#int32) |  | 相机支持的分辨率高度【必填】 |
| fps | [uint32](#uint32) |  | 相机在该分辨率下支持的帧率【必填】 |






<a name="YoYo-YuTooAiPb-V1-EmptyRequest"></a>

### EmptyRequest
EmptyRequest 空请求（入参）






<a name="YoYo-YuTooAiPb-V1-LocalSyncGroupInfo"></a>

### LocalSyncGroupInfo
LocalSyncGroupInfo 本地同步分组信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| groupKey | [string](#string) |  | 分组唯一键 |
| groupName | [string](#string) |  | 分组自定义名称 |
| hostList | [LocalSyncHostInfo](#YoYo-YuTooAiPb-V1-LocalSyncHostInfo) | repeated | 该分组下的本地同步主机信息列表 |






<a name="YoYo-YuTooAiPb-V1-LocalSyncHostInfo"></a>

### LocalSyncHostInfo
LocalSyncHostInfo 本地同步主机信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| instance | [string](#string) |  | 主机唯一实例 |
| port | [uint32](#uint32) |  | 主机服务端口号 |
| groupFlag | [LocalSyncGroupFlagType](#YoYo-YuTooAiPb-V1-LocalSyncGroupFlagType) |  | 分组数据同步标识 |
| groupKey | [string](#string) |  | 分组唯一KEY |
| groupName | [string](#string) |  | 分组自定义名称 |
| systemOS | [string](#string) |  | 主机的系统类型 |
| online | [bool](#bool) |  | 主机是否在线 |
| ipv4List | [string](#string) | repeated | 主机的IPV4地址列表 |
| ipv6List | [string](#string) | repeated | 主机的IPV6地址列表 |






<a name="YoYo-YuTooAiPb-V1-PluFeatInfo"></a>

### PluFeatInfo
PluFeatInfo 商品特征信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| plu | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) |  | 商品信息 |
| featNum | [int32](#int32) |  | 商品特征数量 |






<a name="YoYo-YuTooAiPb-V1-PluInfo"></a>

### PluInfo
PluInfo 商品信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [string](#string) |  | 商品编码【必填】 |
| name | [string](#string) |  | 商品名称【必填】 |






<a name="YoYo-YuTooAiPb-V1-PluMatchInfo"></a>

### PluMatchInfo
PluMatchInfo 商品匹配信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| pluStatusInfo | [PluStatusInfo](#YoYo-YuTooAiPb-V1-PluStatusInfo) |  | 商品状态信息 |
| iconList | [string](#string) | repeated | 商品图片列表 |






<a name="YoYo-YuTooAiPb-V1-PluSaleInfo"></a>

### PluSaleInfo
PluSaleInfo 商品销售信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| sales | [int32](#int32) |  | 销量：重量(克) 、数量【可选】 |
| priceUnit | [PriceUnitType](#YoYo-YuTooAiPb-V1-PriceUnitType) |  | 计价方式【可选】 |
| price | [int32](#int32) |  | 原始单价(分/Kg)【可选】 |
| salePrice | [int32](#int32) |  | 销售单价(分/Kg)【可选】 |
| amount | [int32](#int32) |  | 销售总价（分）【可选】 |
| barCode | [string](#string) |  | 条码【可选】 |
| priceChangeMode | [int32](#int32) |  | 商品是否改价（0-未改价，1-改单价，2-改总价）【可选】 |
| priceType | [int32](#int32) |  | 商品价格类型（1-正常价格，2-临时改价，3-永久改价，4-阶梯变价） |






<a name="YoYo-YuTooAiPb-V1-PluStatusInfo"></a>

### PluStatusInfo
PluStatusInfo 商品状态信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| plu | [PluInfo](#YoYo-YuTooAiPb-V1-PluInfo) |  | 商品信息【必填】 |
| isSale | [bool](#bool) |  | 是否上架【必填】 |
| isLock | [bool](#bool) |  | 是否锁定【必填】 |
| price | [int64](#int64) |  | 商品单价（单位，【计重：分/kg】，【计数：分/个】）【可选】 |
| priceUnit | [PriceUnitType](#YoYo-YuTooAiPb-V1-PriceUnitType) |  | 计价方式【可选】 |
| imageUrl | [string](#string) |  | 商品图片链接【可选】 |
| tareWeight | [int64](#int64) |  | 商品皮重【可选】 |
| expand | [string](#string) |  | 自定义扩展信息【可选】 |






<a name="YoYo-YuTooAiPb-V1-ProductModelInfo"></a>

### ProductModelInfo
ProductModelInfo 产品模型信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| productLevel | [int32](#int32) |  | 产品级别【必填】 |
| productName | [string](#string) |  | 产品级别名称【可选】 |
| aiMode | [AiModeType](#YoYo-YuTooAiPb-V1-AiModeType) |  | AI模型枚举【必填】 |
| aiName | [string](#string) |  | AI模型名称【可选】 |






<a name="YoYo-YuTooAiPb-V1-PublicReply"></a>

### PublicReply
PublicReply 公共响应（出参）


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| code | [ReplyCodeType](#YoYo-YuTooAiPb-V1-ReplyCodeType) |  | 状态码 |
| msg | [string](#string) |  | 状态描述 |






<a name="YoYo-YuTooAiPb-V1-ScheduleInfo"></a>

### ScheduleInfo
ScheduleInfo 进度信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| completed | [uint32](#uint32) |  | 已完成学习的图片数量 |
| total | [uint32](#uint32) |  | 需要学习的图片总数量 |






<a name="YoYo-YuTooAiPb-V1-ShopBaseInfo"></a>

### ShopBaseInfo
ShopBaseInfo 门店基础信息


| Field | Type | Label | Description |
| ----- | ---- | ----- | ----------- |
| name | [string](#string) |  | 门店名称【可选】 |
| contact | [string](#string) |  | 门店联系人【可选】 |
| phone | [string](#string) |  | 联系人电话【可选】 |





 


<a name="YoYo-YuTooAiPb-V1-AiModeType"></a>

### AiModeType
AiModeType AI模型枚举类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| GENERAL | 0 | 通用模型（适用于生鲜和零食混用场景） |
| FRESH | 1 | 生鲜模型（适用于生鲜场景） |
| SNACK | 2 | 零食模型（适用于零食场景） |



<a name="YoYo-YuTooAiPb-V1-AiWeightJudgeModeType"></a>

### AiWeightJudgeModeType
AiWeightJudgeModeType AI重量识别逻辑判断模式

| Name | Number | Description |
| ---- | ------ | ----------- |
| ModeRush | 0 | 急速模式（识别速度快，精度一般） |
| ModeFast | 1 | 快速模式（识别速度较快，精度较好） |
| ModeMedium | 2 | 中速模式（识别速度较慢，精度好） |
| ModeSlow | 3 | 慢速模式（识别速度慢，精度极好） |



<a name="YoYo-YuTooAiPb-V1-GetStudyDataModeType"></a>

### GetStudyDataModeType
GetStudyDataModeType 获取学习数据时的模式枚举类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| AllMode | 0 | 全量导出 |
| NewFeat | 1 | 只导出指定时间后的新特征 |
| NewPlu | 2 | 只导出指定时间后的新商品 |



<a name="YoYo-YuTooAiPb-V1-LocalSyncGroupFlagType"></a>

### LocalSyncGroupFlagType
LocalSyncGroupFlagType 本地同步分组数据同步标识类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| DisableSync | 0 | 禁用数据同步 |
| TwoWaySync | 1 | 数据双向同步 |
| OnlyReceiveSync | 2 | 仅接收数据 |
| OnlySendSync | 3 | 仅发送数据 |



<a name="YoYo-YuTooAiPb-V1-MarkType"></a>

### MarkType
MarkType 点选标记方式

| Name | Number | Description |
| ---- | ------ | ----------- |
| NoAction | 0 | 无后续操作 |
| MatchOut | 2 | 在识别结果中选择 |
| Shortcut | 3 | 快捷方式点选 |
| Search | 4 | 在搜索结果中选择 |
| StudyMode | 9 | 学习模式 |



<a name="YoYo-YuTooAiPb-V1-MatchingType"></a>

### MatchingType
MatchingType 识别类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| Auto | 0 | 自动识别 |
| Forcibly | 1 | 强制识别 |



<a name="YoYo-YuTooAiPb-V1-PerformanceModeType"></a>

### PerformanceModeType
PerformanceModeType 性能模式枚举

| Name | Number | Description |
| ---- | ------ | ----------- |
| BalancedMode | 0 | 均衡模式(占用CPU: 50%) |
| EfficientMode | 1 | 高效模式(占用CPU: 100%) |



<a name="YoYo-YuTooAiPb-V1-PluSyncModeType"></a>

### PluSyncModeType
PluSyncModeType 商品同步模式类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| Full | 0 | 全量同步 |
| Demo | 1 | 演示商品 |
| Append | 2 | 增量同步 |



<a name="YoYo-YuTooAiPb-V1-PriceUnitType"></a>

### PriceUnitType
PriceUnitType 计价方式类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| Weight | 0 | 按重量计价 |
| Number | 1 | 按数量计价 |



<a name="YoYo-YuTooAiPb-V1-ReplyCodeType"></a>

### ReplyCodeType
ReplyCodeType 响应码枚举

| Name | Number | Description |
| ---- | ------ | ----------- |
| ErrUnknown | 0 | 默认的未知错误 |
| Success | 1 | 默认的操作成功 |
| ErrNotInit | 1001 | 程序未初始化 |
| ErrInit | 1002 | 程序初始化失败 |
| ErrParams | 1003 | 接口参数错误 |
| ErrVersionInfo | 1004 | 程序版本信息异常 |
| ErrSetConfig | 1005 | 修改配置信息失败 |
| ErrNotSupport | 1006 | 不支持的操作 |
| ErrReplaceSN | 2001 | 新旧SN替换错误 |
| ErrNotActivate | 2002 | 设备未激活 |
| ErrActivate | 2003 | 设备激活失败 |
| ErrDeviceInfo | 2004 | 设备信息异常 |
| ErrActivateExpired | 2005 | 测试激活码已过期 |
| ErrSystemTime | 2006 | 系统时间异常 |
| ErrNotLoadSo | 3001 | SO库未加载 |
| ErrLoadSo | 3002 | SO库加载失败 |
| ErrNotInitSo | 3003 | SO库未初始化 |
| ErrInitSo | 3004 | SO库初始化失败 |
| ErrAi | 3005 | 识别异常 |
| ErrNotIdentify | 3006 | 无识别记录 |
| ErrRecogModel | 3600 | 识别模型出错 |
| ErrRecogModelValidation | 3601 | 识别模型加密库出错 |
| ErrRecogModelAcceleration | 3602 | 识别模型加速库出错 |
| ErrRecogModelPreprocess | 3603 | 识别模型预处理阶段出错 |
| ErrRecogModelPostprocess | 3604 | 识别模型后处理阶段出错 |
| ErrRecogModelParam | 3605 | 识别模型传入参数错误 |
| ErrRecogModelMemory | 3606 | 识别模型内存错误 |
| ErrRecogModelData | 3607 | 识别模型数据错误 |
| ErrRecogModelPipiline | 3608 | 识别模型调用流程错误 |
| ErrRecogModelFile | 3609 | 识别模型文件错误 |
| ErrRecogModelKeyEmpty | 3610 | 识别模型键不存在错误 |
| ErrMatchRuleNeedSync | 3801 | 匹配算法需要同步商品数据 |
| ErrMatchRuleParam | 3802 | 匹配算法传入参数错误 |
| ErrMatchRuleMemory | 3803 | 匹配算法内存错误 |
| ErrMatchRuleData | 3804 | 匹配算法数据错误 |
| ErrMatchRulePipiline | 3805 | 匹配算法调用流程错误 |
| ErrMatchRuleFile | 3806 | 匹配算法文件错误 |
| ErrMatchRuleFileEmpty | 3807 | 匹配算法导出文件为空 |
| ErrCamera | 4001 | 相机异常 |
| ErrImage | 4002 | 图像处理异常 |
| ErrInitPluData | 5001 | 初始化商品数据异常 |
| ErrInitIconMatch | 5002 | 初始化ICON匹配服务异常 |
| ErrIconMatch | 5003 | 商品ICON匹配异常 |
| ErrPluNotExist | 5004 | 商品不存在 |
| ErrPluLock | 5005 | 商品已锁定 |
| ErrPluSetInfo | 5006 | 商品信息修改失败 |
| ErrNetwork | 6001 | 网络异常 |



<a name="YoYo-YuTooAiPb-V1-SetStudyDataModeType"></a>

### SetStudyDataModeType
SetStudyDataModeType 配置学习数据模式类型

| Name | Number | Description |
| ---- | ------ | ----------- |
| MERGER | 0 | 合并模式（仅合并已经存在，但未学习过的商品的学习数据） |
| Merger | 0 | 合并模式（仅合并已经存在，但未学习过的商品的学习数据） |
| REPLACE | 1 | 全量覆盖模式（增量导出的学习数据请勿使用） |
| Replace | 1 | 全量覆盖模式（增量导出的学习数据请勿使用） |
| MergerAppend | 2 | 合并追加模式（不管商品是否存在，都为未学习过的商品赋值学习数据） |
| MultipleMerger | 3 | 学习数据特征合并模式（合入本机不存在的商品或本机商品特征不足20条的学习数据） |



<a name="YoYo-YuTooAiPb-V1-WeightStatusType"></a>

### WeightStatusType
WeightStatusType 重量稳定状态

| Name | Number | Description |
| ---- | ------ | ----------- |
| Unstable | 0 | 不稳定 |
| Stable | 1 | 稳定 |
| Error | 2 | 错误 |
| Overload | 3 | 超载 |
| Underload | 4 | 欠载 |
| InternalAuto | 5 | 内部自动处理 |


 

 

 



## Scalar Value Types

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |

