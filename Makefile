go-plugin:
	go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest

go-server:
	protoc -I. \
	--go_out=build/go-server \
	--go-grpc_out=build/go-server \
	--grpc-gateway_out=build/go-server \
	server/*.proto \
	shared/*.proto

go-client:
	protoc -I. \
	--go_out=build/go-client \
	--go-grpc_out=build/go-client \
	--grpc-gateway_out=build/go-client \
	client/*.proto \
	shared/*.proto

java:
	protoc -I. \
	--java_out=build/java \
	--grpc-java_out=build/java \
	shared/*.proto \
	client/*.proto

csharp:
	protoc -I. \
	--plugin=protoc-gen-grpc=C:\\Users\\Bearki\\.nuget\\packages\\grpc.tools\\2.49.1\\tools\\windows_x64\\grpc_csharp_plugin.exe \
	--csharp_out=build/csharp \
	--grpc_out=build/csharp \
	--grpc_opt=no_server \
	shared/*.proto \
	client/*.proto

flutter-plugin:
	flutter pub global activate protoc_plugin

dart-plugin:
	dart pub global activate protoc_plugin

flutter dart:
	protoc -I. \
	--dart_out=grpc:build/dart \
	shared/*.proto \
	client/*.proto

nodejs:
	protoc -I. \
	--plugin=protoc-gen-grpc=node/node_modules/grpc-tools/bin/grpc_node_plugin.exe \
	--js_out=import_style=commonjs,binary:node/src \
	--grpc_out=build/node \
	shared/*.proto \
	client/*.proto